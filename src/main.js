import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import HomePage from './components/HomePage'
import Configuration from "./components/Configuration";
import CreateApiary from './components/CRUD'
import VisitApiary from "./components/VisitApiary";

Vue.use(VueRouter)

const router = new VueRouter({
  mode : 'history',
  routes: [
      {
        path: '/',
        component: HomePage
      },
      {
        path: '/configuration',
        component: Configuration
      },
      {
          path: '/apiary/create',
          component: CreateApiary
      },
      {
          path: '/apiary/visit/:id',
          component: VisitApiary
      },
  ]
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
