import VueRouter from "vue-router";
import HomePage from '../components/HomePage'
import Configuration from "../components/Configuration";


export default new VueRouter({
    mode : 'history',
    routes: [
            {
            path: '/',
            component: HomePage
        },
    ]
})
